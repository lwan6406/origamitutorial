// pop-up window

function alertPop() {
	layer.msg('New features coming soon..<br>See you next time!', {icon:0}); 
}

function uploadNew() {

	layer.open({
		type: 1,
		title: false,
		area: '26vw;',
		shade: 0.8,
		id: 'origami',
		resize: false,
		btn: ['Upload', 'Cancel'],
		btnAlign: 'c',
		moveType: 1,
		content: $('#uploadwindow'),
		success: function(layero){
		    var btn = layero.find('.layui-layer-btn');
		    btn.find('.layui-layer-btn0').attr({target: '_blank',target: '_blank'});
		}
	});
}

function alertTut() {
	layer.msg('This tutorial is still on its way..<br>See you next time!', {icon:5});
}

function about() {
	layer.open({
		type: 1,
	 	title: 'About',
	 	closeBtn: true,
	  	area: '300px;',
	  	shade: 0.8,
	 	resize: false,
	 	
	 	btnAlign: 'c',
	 	moveType: 0,
	  	content: '<div style="padding: 5vw; line-height: 4vw; background-color: #6a5c5c; color: #fff; font-weight: 300;">Contact Us<br>E-mail:loveorigami@gmail.com<br>Tel: +61 432 123 456</div>',
	  	
	});
}

function alertAbout() {
	layer.msg('Contact Us<br>E-mail: loveorigami@gmail.com<br>Tel: +61 432 123 456<br>(Close automatically in 5s.)', {icon:6,time:5000}); 
}

